package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/models"
	"gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/styles"
)

var helpSymbolsCmd = &cobra.Command{
	Use:   "help-symbols",
	Short: "Explain all the symbols used in the output",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Various symbols and emojis are used in the human-readable output to make it easier to read. Here is a list of all the symbols and their meanings:")
		fmt.Println()
		fmt.Println(styles.ColorForegroundStrong.Render("Status symbols:"))
		fmt.Println()
		fmt.Println(styles.IndentOne.Render(models.SymbolAccountDisabled + " account is disabled."))
		fmt.Println(styles.IndentOne.Render(models.SymbolPasswordExpired + " account is expired."))
		fmt.Println(styles.IndentOne.Render(models.SymbolCertificateFromToday + " a certificate from GÉANT TCS/Sectigo is valid starting today (only needed for KIT-CA)."))
		fmt.Println(styles.IndentOne.Render(styles.ColorRed.Render(models.SymbolSendLimit) + " indicates that the account has a send limit, usually set to 0 to disable sending emails."))
		fmt.Println()
		fmt.Println(styles.ColorForegroundStrong.Render("Account Types:"))
		fmt.Println()
		fmt.Println(styles.IndentOne.Render(models.SymbolTypeKITStaff + " account belongs to a KIT staff member."))
		fmt.Println(styles.IndentOne.Render(models.SymbolTypeKITGuP + " account belongs to a guest or partner (GuP)."))
		fmt.Println(styles.IndentOne.Render(models.SymbolTypeKITStudent + " account belongs to a student. " + models.SymbolPseudonym + " is added if the account does not contain personal data („pseudonymisiert“)."))
		fmt.Println(styles.IndentOne.Render(models.SymbolTypeKITAlumni + " account belongs to an Alumni."))
		fmt.Println(styles.IndentOne.Render(models.SymbolTypeKITFunction + " account is a function account (account owner is indicated by " + models.SymbolResponsiblePerson + ")."))
		fmt.Println(styles.IndentOne.Render(models.SymbolTypeKITGroup + " account is a group account."))
		fmt.Println(styles.IndentOne.Render(models.SymbolTypeKITContact + " account is a contact; usually used to redirect a specific email address."))
		fmt.Println(styles.IndentOne.Render(models.SymbolTypeKITUnknown + " unknown account type. Most likely a non-managed AD-only account."))
		fmt.Println()
		fmt.Println(styles.ColorForegroundStrong.Render("Misc:"))
		fmt.Println(styles.IndentOne.Render(models.SymbolCertificate + " each certificate starts with this symbol."))
		fmt.Println(styles.IndentOne.Render(models.SymbolITB + " account belongs to an IT officer (ITB)."))
		fmt.Println(styles.IndentOne.Render(models.SymbolEmptyString + " indicates that a field is empty or not set."))
		fmt.Println(styles.IndentOne.Render(models.SymbolRedirect + " emails to this account are redirected to the indicated address."))
		fmt.Println(styles.IndentOne.Render(models.SymbolRedirectAlt + " indirect redirect for non-person accounts."))
		fmt.Println(styles.IndentOne.Render(models.SymbolBulletPointPrimary + " marks the primary email address. Secondary email addresses use " + models.SymbolBulletPoint + "."))
		os.Exit(0)
	},
	Args: cobra.NoArgs,
}

func init() {
	rootCmd.AddCommand(helpSymbolsCmd)
}
