package cmd

import (
	"bufio"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"

	"github.com/go-ldap/ldap/v3"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/models"
	"gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/styles"
)

var (
	cfgFile string
)

// init sets up the command line flags and configuration defaults
func init() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)

	viper.SetConfigName(".kituserlookup") // name of config file (without extension)
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file")

	// define global flags
	rootCmd.PersistentFlags().BoolP("json", "j", false, "output JSON")
	rootCmd.PersistentFlags().BoolP("memberof", "m", false, "print all memberOf values")
	rootCmd.PersistentFlags().Bool("noMembers", false, "don't look up and display members of groups")
	rootCmd.PersistentFlags().StringP("filter", "f", "default", "use specific LDAP filter ["+models.LDAPFilterShortestPrefix.FancyString(styles.ColorForegroundStrong)+"]")
	_ = viper.BindPFlag("json", rootCmd.PersistentFlags().Lookup("json"))
	_ = viper.BindPFlag("memberof", rootCmd.PersistentFlags().Lookup("memberof"))
	_ = viper.BindPFlag("noMembers", rootCmd.PersistentFlags().Lookup("noMembers"))
	_ = viper.BindPFlag("filter", rootCmd.PersistentFlags().Lookup("filter"))
	viper.RegisterAlias("ldap.filtertemplate", "filter")

	// set flag defaults
	viper.SetDefault("json", false)
	viper.SetDefault("memberof", false)
	viper.SetDefault("filter", "default")

	// set config file defaults
	viper.SetDefault("ldap.host", "kit-ad.scc.kit.edu")
	viper.SetDefault("ldap.port", 636)
	viper.SetDefault("ldap.basedn", "DC=kit,DC=edu")

	// enhance shell completions
	_ = rootCmd.RegisterFlagCompletionFunc("filter", func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return models.LDAPFilterShortestPrefix.LongestFirst, cobra.ShellCompDirectiveDefault
	})
}

// initConfig is run after init() and before the command is executed
// It reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		viper.SetConfigType("toml")
		viper.SetConfigName("kituserlookup")
		viper.AddConfigPath("/etc")

		// look in home directory if applicable
		home, err := os.UserHomeDir()
		if err == nil {
			viper.AddConfigPath(path.Join(home, ".config"))
		}
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	err := viper.ReadInConfig()
	if err != nil {
		log.Println(err)
	}
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "kituserlookup",
	Short: "Look up account information from KIT Active Directory",
	Args:  cobra.MinimumNArgs(1),
	Run: func(_cmd *cobra.Command, args []string) {
		results, err := doLDAPSearch(strings.Join(args, " "))
		if err != nil {
			log.Fatal(err)
		}
		// display results
		if viper.GetBool("json") {
			jb, _ := json.MarshalIndent(results, "", "  ")
			fmt.Println(string(jb))
		} else {
			for idx, result := range results {
				result.PrettyPrint()
				if idx < len(results)-1 {
					fmt.Println()
				}
			}
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	rootCmd.AddCommand(CompletionCmd)

	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

// doLDAPSearch performs an LDAP search and parses the results
func doLDAPSearch(queryString string) ([]models.SearchResult, error) {
	if !viper.IsSet("ldap.user") {
		log.Fatal("No LDAP user set")
	}
	// get LDAP password
	SetLDAPPassword()

	// connect to LDAP
	connString := fmt.Sprintf("%s:%d", viper.Get("ldap.host"), viper.Get("ldap.port"))
	conn, err := ldap.DialTLS("tcp", connString,
		&tls.Config{
			ServerName: viper.Get("ldap.host").(string),
			MinVersion: tls.VersionTLS12,
		})
	if err != nil {
		log.Fatal(err)
	}
	// bind to LDAP
	err = conn.Bind(viper.Get("ldap.user").(string), viper.Get("ldap.pass").(string))
	if err != nil {
		log.Fatal(err)
	}
	// search LDAP
	results, err := models.SearchLDAP(
		conn,
		models.GetLDAPFilter(viper.GetString("filter"), queryString),
		!viper.GetBool("noMembers"))
	return results, err
}

// SetLDAPPassword sets the LDAP password from the config file or from a command
func SetLDAPPassword() {
	if !viper.IsSet("ldap.pass") {
		if !viper.IsSet("ldap.passcmd") {
			log.Fatal("No LDAP password or password command set")
		}
		passCmd, err := exec.LookPath(viper.GetStringSlice("ldap.passcmd")[0])
		if err != nil {
			log.Fatalf("Password command »%s« not found: %s", passCmd, err)
		}
		cmd := exec.Command(passCmd, viper.GetStringSlice("ldap.passcmd")[1:]...)
		stdout, err := cmd.StdoutPipe()
		if err != nil {
			log.Fatal(err)
		}
		if err := cmd.Start(); err != nil {
			log.Fatal(err)
		}
		pass, err := bufio.NewReader(stdout).ReadString('\n')
		if err != nil {
			log.Fatalf("Unable to read stdout from PassCmd: %s", err)
		}
		if err := cmd.Wait(); err != nil {
			log.Fatal(err)
		}
		viper.Set("ldap.pass", strings.TrimSuffix(pass, "\n"))
	}
}
