package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Show version and build information",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("kituserlookup %s, commit %s, built at %s\n", version, commit, date)
		os.Exit(0)
	},
	Args: cobra.NoArgs,
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
