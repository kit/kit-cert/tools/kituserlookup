package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/models"
	"gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/styles"
)

var helpFiltersCmd = &cobra.Command{
	Use:   "help-filters",
	Short: "Explain LDAP filters and their meanings",
	Run: func(_cmd *cobra.Command, args []string) {
		fmt.Println("LDAP filters determine how accounts are found from the search query. Several filters with different focus are included. To get the exact semantics, please refer to the source code [1].")
		fmt.Println("The default filter is usually sufficient for most use cases.")
		fmt.Println()
		fmt.Println(styles.ColorForegroundStrong.Render("Available filters"))

		var helptext strings.Builder
		for idx, name := range models.LDAPFilterNamesSorted {
			helptext.WriteString(models.SymbolBulletPoint + " " + styles.ColorForegroundStrong.Italic(true).Render(name) + ": " + models.LDAPFilter[name].Description)
			if idx < len(models.LDAPFilterNamesSorted)-1 {
				helptext.WriteString("\n")
			}
		}
		fmt.Print(styles.HelpText.Render(helptext.String()))
		fmt.Println()

		fmt.Println("[1] " + styles.ColorBlue.Render("https://gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/-/blob/main/models/ldapFilter.go?ref_type=heads"))
		fmt.Println("[2] " + styles.ColorBlue.Render(("https://intranet.kit.edu/kv-suche.php")))

		os.Exit(0)
	},
	Args: cobra.NoArgs,
}

func init() {
	rootCmd.AddCommand(helpFiltersCmd)
}
