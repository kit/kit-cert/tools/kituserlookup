package styles

import "github.com/charmbracelet/lipgloss"

// colors are taken from gruvbox (https://github.com/morhetz/gruvbox)
var (
	ColorForegroundStrong = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{ // fg0
		Light: lipgloss.CompleteColor{
			TrueColor: "#282828",
			ANSI256:   "235",
			ANSI:      "15", // -
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#fbf1c7",
			ANSI256:   "229",
			ANSI:      "15", // -
		},
	})
	ColorForeground = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{ // fg
		Light: lipgloss.CompleteColor{
			TrueColor: "#3c3836",
			ANSI256:   "237",
			ANSI:      "15",
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#ebdbb2",
			ANSI256:   "223",
			ANSI:      "15",
		},
	})
	ColorForegroundGray = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{ // gray
		Light: lipgloss.CompleteColor{
			TrueColor: "#928374",
			ANSI256:   "244",
			ANSI:      "8",
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#928374",
			ANSI256:   "245",
			ANSI:      "8",
		},
	})
	ColorBackground = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{ // bg1
		Light: lipgloss.CompleteColor{
			TrueColor: "#ebdbb2",
			ANSI256:   "223",
			ANSI:      "8", // -
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#3c3836",
			ANSI256:   "237",
			ANSI:      "8", // -
		},
	})
	ColorRed = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{ // red
		Light: lipgloss.CompleteColor{
			TrueColor: "#cc241d",
			ANSI256:   "128",
			ANSI:      "1",
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#cc241d",
			ANSI256:   "124",
			ANSI:      "1",
		},
	})
	ColorOrange = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{
		Light: lipgloss.CompleteColor{
			TrueColor: "#d65d0e",
			ANSI256:   "130",
			ANSI:      "1", // -
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#d65d0e",
			ANSI256:   "166",
			ANSI:      "1", // -
		},
	})
	ColorOrangeAccent = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{
		Light: lipgloss.CompleteColor{
			TrueColor: "#af3a03",
			ANSI256:   "130",
			ANSI:      "1", // -
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#fe8019",
			ANSI256:   "208",
			ANSI:      "1", // -
		},
	})
	ColorYellow = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{
		Light: lipgloss.CompleteColor{
			TrueColor: "#f79921",
			ANSI256:   "172",
			ANSI:      "3",
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#f79921",
			ANSI256:   "172",
			ANSI:      "3",
		},
	})
	ColorGreen = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{
		Light: lipgloss.CompleteColor{
			TrueColor: "#98971a",
			ANSI256:   "106",
			ANSI:      "2",
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#98971a",
			ANSI256:   "106",
			ANSI:      "2",
		},
	})
	ColorCyan = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{
		Light: lipgloss.CompleteColor{
			TrueColor: "#689d6a",
			ANSI256:   "72",
			ANSI:      "6",
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#689d6a",
			ANSI256:   "72",
			ANSI:      "6",
		},
	})
	ColorBlue = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{
		Light: lipgloss.CompleteColor{
			TrueColor: "#458588",
			ANSI256:   "66",
			ANSI:      "4",
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#458588",
			ANSI256:   "66",
			ANSI:      "4",
		},
	})
	ColorPurple = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{ // purple alternative
		Light: lipgloss.CompleteColor{
			TrueColor: "#8f3f71",
			ANSI256:   "96",
			ANSI:      "13",
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#d3869b",
			ANSI256:   "175",
			ANSI:      "13",
		},
	})
	ColorAqua = lipgloss.NewStyle().Foreground(lipgloss.CompleteAdaptiveColor{ // aqua alternative
		Light: lipgloss.CompleteColor{
			TrueColor: "#427b58",
			ANSI256:   "66",
			ANSI:      "14",
		},
		Dark: lipgloss.CompleteColor{
			TrueColor: "#8ec07c",
			ANSI256:   "108",
			ANSI:      "14",
		},
	})

	TextBold    = lipgloss.NewStyle().Bold(true)
	TextItalic  = lipgloss.NewStyle().Italic(true)
	IndentOne   = lipgloss.NewStyle().PaddingLeft(2)
	IndentTwo   = lipgloss.NewStyle().PaddingLeft(4)
	IndentThree = lipgloss.NewStyle().PaddingLeft(6)

	HelpText = lipgloss.NewStyle().
			Align(lipgloss.Left).
			Padding(1, 0, 1, 2)
)
