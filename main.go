package main

import "gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/cmd"

func main() {
	cmd.Execute()
}
