package models

import (
	"crypto/x509"
	"encoding/json"
	"math/big"
	"time"
)

type UserCertificate x509.Certificate

func (u *UserCertificate) MarshalJSON() ([]byte, error) {
	var cert struct {
		Serial         *big.Int  `json:"serial"`
		HexSerial      string    `json:"hexSerial"`
		Issuer         string    `json:"issuer"`
		Subject        string    `json:"subject"`
		NotBefore      time.Time `json:"notBefore"`
		NotAfter       time.Time `json:"notAfter"`
		EmailAddresses []string  `json:"emailAddresses,omitempty"`
		DNSNames       []string  `json:"dnsNames,omitempty"`
	}
	cert.Serial = u.SerialNumber
	cert.HexSerial = u.SerialNumber.Text(16)
	cert.Issuer = u.Issuer.String()
	cert.Subject = u.Subject.String()
	cert.NotBefore = u.NotBefore
	cert.NotAfter = u.NotAfter
	cert.EmailAddresses = u.EmailAddresses
	cert.DNSNames = u.DNSNames
	return json.Marshal(cert)
}
