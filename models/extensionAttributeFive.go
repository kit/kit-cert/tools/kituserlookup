package models

import "encoding/json"

type ExtensionAttributeFive int

const (
	E5None ExtensionAttributeFive = iota
	E5KITFunction
	E5KITGuP
	E5KITStaff
	E5KITStudent
	E5KITAlumni  // fake state, this is not part of extensionAttribute5
	E5KITGroup   // fake state, this is not part of extensionAttribute5
	E5KITContact // fake state, this is not part of extensionAttribute5
)

func (e *ExtensionAttributeFive) ToString() string {
	switch *e {
	case E5KITFunction:
		return "Function"
	case E5KITGuP:
		return "GuP"
	case E5KITStaff:
		return "Staff"
	case E5KITStudent:
		return "Student"
	default:
		return "None"
	}
}

func (e *ExtensionAttributeFive) ToEmoji() string {
	switch *e {
	case E5KITFunction:
		return SymbolTypeKITFunction
	case E5KITGuP:
		return SymbolTypeKITGuP
	case E5KITStaff:
		return SymbolTypeKITStaff
	case E5KITStudent:
		return SymbolTypeKITStudent
	case E5KITAlumni:
		return SymbolTypeKITAlumni
	case E5KITGroup:
		return SymbolTypeKITGroup
	case E5KITContact:
		return SymbolTypeKITContact
	default:
		return SymbolTypeKITUnknown
	}
}

func (u *ExtensionAttributeFive) MarshalJSON() ([]byte, error) {
	return json.Marshal(u.ToString())
}
