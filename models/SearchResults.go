package models

import (
	"cmp"
	"crypto/x509"
	"fmt"
	"log"
	"regexp"
	"slices"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/charmbracelet/lipgloss"

	"github.com/go-ldap/ldap/v3"

	"gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/styles"

	"github.com/spf13/viper"
)

var (
	wantedLDAPAttributes = []string{
		"distinguishedName",
		"cn",
		"description",
		"department",
		"displayName",
		"extensionAttribute5",
		"extensionAttribute9",
		"givenName",
		"memberOf",
		"member",
		"physicalDeliveryOfficeName",
		"objectClass",
		"proxyAddresses",
		"sAMAccountName",
		"sn",
		"submissionContLength",
		"targetAddress",
		"telephoneNumber",
		"userAccountControl",
		"userCertificate",
		"userPrincipalName",
	}
	//matchStudent    = regexp.MustCompile(`u[[:alpha:]][[:digit:]]{4}`)
	matchITBOE      = regexp.MustCompile(`CN=SCC-ITB-(?P<OE>[^,]+)`)
	ITBSuffixesNoOE = map[string]bool{
		"CN=SCC-ITB-biju":                                 true,
		"CN=SCC-ITB-gesamt":                               true,
		"CN=SCC-ITB-gesamt-SndTo":                         true,
		"CN=SCC-ITB-gruppenverwalter":                     true,
		"CN=SCC-ITB-Manager":                              true,
		"CN=SCC-ITB-mitAdminKonto":                        true,
		"CN=SCC-ITB-mitAdminKontoPortal-Wissenswertes_ro": true,
		"CN=SCC-ITB-Portal_ro":                            true,
		"CN=SCC-ITB-Portal-Wissenswertes_ro":              true,
		"CN=SCC-ITB-XYZTEST":                              true,
		"CN=SCC-ITB-ZZZ":                                  true,
		"CN=SCC-ITB-ZZZ_KS":                               true,
	}
)

type SearchResult struct {
	CN                         string                 `json:"cn,omitempty"`
	DN                         string                 `json:"distinguishedName,omitempty"`
	Description                string                 `json:"description,omitempty"`
	Department                 string                 `json:"department,omitempty"`
	DisplayName                string                 `json:"displayName,omitempty"`
	GivenName                  string                 `json:"givenName,omitempty"`
	MemberOf                   []string               `json:"memberOf,omitempty"`
	PhysicalDeliveryOfficeName string                 `json:"physicalDeliveryOfficeName,omitempty"`
	SAMAccountName             string                 `json:"sAMAccountName,omitempty"` //nolint:tagliatelle
	SN                         string                 `json:"sn,omitempty"`
	TargetAddress              string                 `json:"targetAddress,omitempty"`
	TelephoneNumber            string                 `json:"telephoneNumber,omitempty"`
	E5                         ExtensionAttributeFive `json:"extensionAttribute5,omitempty"`
	E9                         string                 `json:"extensionAttribute9,omitempty"`
	AccountType                ExtensionAttributeFive `json:"accountType,omitempty"`
	ObjectClass                []string               `json:"objectClass,omitempty"`
	SubmissionContLength       *int                   `json:"submissionContLength,omitempty"`
	UserPrincipalName          string                 `json:"userPrincipalName,omitempty"`
	UserAccountControl         UAC                    `json:"userAccountControl,omitempty"`
	UserCertificate            []UserCertificate      `json:"userCertificate,omitempty"`
	ITB                        []string               `json:"itb,omitempty"`
	IsPseudonym                bool                   `json:"isPseudonym,omitempty"`
	Members                    *[]SearchResult        `json:"members,omitempty"`
	ProxyAddresses             struct {
		Primary   string   `json:"primary,omitempty"`
		Secondary []string `json:"secondary,omitempty"`
	} `json:"proxyAddresses,omitempty"`
}

func SearchLDAP(conn *ldap.Conn, queryString string, getMembers bool) ([]SearchResult, error) {
	// search LDAP
	query := ldap.NewSearchRequest(
		viper.Get("ldap.basedn").(string),
		ldap.ScopeWholeSubtree,
		ldap.DerefAlways,
		0, 0, false,
		queryString,
		wantedLDAPAttributes,
		nil)
	searchResults, err := conn.Search(query)
	if err != nil {
		return nil, err
	}
	// parse results
	results := make([]SearchResult, 0)
	for _, entry := range searchResults.Entries {
		result := SearchResult{
			DN: entry.DN,
		}
		rawMembers := make([]string, 0)
		hasAlumniMailAddress := false
		for _, attr := range entry.Attributes {
			switch attr.Name {
			case "cn":
				result.CN = attr.Values[0]
			case "description":
				result.Description = attr.Values[0]
			case "department":
				result.Department = attr.Values[0]
			case "displayName":
				result.DisplayName = attr.Values[0]
			case "extensionAttribute5":
				switch attr.Values[0] {
				case "kit-function":
					result.E5 = E5KITFunction
				case "kit-misc-staff":
					result.E5 = E5KITGuP
				case "kit-staff":
					result.E5 = E5KITStaff
				case "kit-student":
					result.E5 = E5KITStudent
				default:
					result.E5 = E5None
				}
			case "extensionAttribute9":
				result.E9 = attr.Values[0]
			case "givenName":
				result.GivenName = attr.Values[0]
			case "memberOf":
				result.MemberOf = append(result.MemberOf, attr.Values...)
				// extract ITB OEs
				for _, group := range attr.Values {
					matches := matchITBOE.FindStringSubmatch(group)
					// remove SCC-ITB-gesamt
					matches = slices.DeleteFunc(matches, func(s string) bool {
						return ITBSuffixesNoOE[s]
					})
					if len(matches) > 1 {
						result.ITB = append(result.ITB, matches[1])
					}
				}
			case "member":
				if getMembers {
					rawMembers = append(rawMembers, attr.Values...)
				}
			case "physicalDeliveryOfficeName":
				if attr.Values[0] != "B- R- -" {
					result.PhysicalDeliveryOfficeName = attr.Values[0]
				}
			case "objectClass":
				result.ObjectClass = attr.Values
			case "proxyAddresses":
				for _, addr := range attr.Values {
					switch {
					// is alumni mail address?
					case strings.Contains(addr, "@alumni.kit.edu") || strings.Contains(addr, "@alumni.uni-karlsruhe.de"):
						hasAlumniMailAddress = true
						fallthrough
					// primary address
					case strings.HasPrefix(addr, "SMTP:"):
						result.ProxyAddresses.Primary = strings.TrimPrefix(addr, "SMTP:")
					// secondary addresses
					case strings.HasPrefix(addr, "smtp:"):
						result.ProxyAddresses.Secondary = append(result.ProxyAddresses.Secondary, strings.TrimPrefix(addr, "smtp:"))
					}
				}
			case "sAMAccountName":
				result.SAMAccountName = attr.Values[0]
			case "sn":
				result.SN = attr.Values[0]
			case "submissionContLength":
				scl, err := strconv.Atoi(attr.Values[0])
				if err == nil {
					result.SubmissionContLength = &scl
				}
			case "targetAddress":
				if strings.HasPrefix(strings.ToLower(attr.Values[0]), "smtp:") {
					result.TargetAddress = strings.Split(attr.Values[0], ":")[1] // handle lower and uppercase prefix
				} else {
					result.TargetAddress = attr.Values[0]
				}
			case "telephoneNumber":
				result.TelephoneNumber = attr.Values[0]
			case "userAccountControl":
				result.UserAccountControl = UACFromLDAPString(attr.Values[0])
			case "userCertificate":
				for idx := range attr.Values {
					c, err := x509.ParseCertificate([]byte(attr.Values[idx]))
					if err == nil {
						result.UserCertificate = append(result.UserCertificate, UserCertificate(*c))
					}
				}
			case "userPrincipalName":
				result.UserPrincipalName = attr.Values[0]
			}
			// parse members (now that DN is known)
			if getMembers {
				result.Members = new([]SearchResult)
				for _, member := range rawMembers {
					members, err := SearchLDAP(
						conn,
						GetLDAPFilter("distinguishedName", member),
						false)
					if err != nil {
						// TODO: change into ignore after debugging
						log.Fatal(err)
					}
					*result.Members = append(*result.Members, members...)
				}
			}
		}

		// sort secondary addresses
		slices.SortStableFunc(result.ProxyAddresses.Secondary, func(a, b string) int {
			A := strings.ToLower(a)
			B := strings.ToLower(b)
			// always sort sAMAccountName@domain last
			crapEmail := strings.ToLower(result.SAMAccountName) + "@"

			switch {
			case strings.HasPrefix(A, crapEmail):
				return 1
			case strings.HasPrefix(B, crapEmail):
				return -1
			default:
				return cmp.Compare(A, B)
			}
		})

		// make map of objectClass for O(1) access
		ocMap := make(map[string]bool)
		for _, oc := range result.ObjectClass {
			ocMap[oc] = true
		}
		// derive AccountType from extensionAttribute5 and group membership
		// default to extensionAttribute5
		result.AccountType = result.E5
		// check groups if extensionAttribute5 is not set
		if result.AccountType == E5None {
		checkMemberLoop:
			for _, group := range result.MemberOf {
				switch group {
				case "CN=SCC-Servicekonten-IDM,OU=Groups,OU=SCC,OU=Staff,OU=KIT,DC=kit,DC=edu":
					result.AccountType = E5KITFunction
					break checkMemberLoop
				}
			}
		}
		// check if user is a contact
		if ocMap["contact"] {
			result.AccountType = E5KITContact
		}
		// check if user is a group
		if ocMap["group"] {
			result.AccountType = E5KITGroup
		}
		// check if user is alumni
		if hasAlumniMailAddress { // was: strings.HasSuffix(result.DN, "OU=ALUMNI,OU=Meta,OU=KIT,DC=kit,DC=edu")
			result.AccountType = E5KITAlumni
		}
		// check if user's personal data is hidden („pseudonymisiert“)
		// last check is the long version of: result.GivenName == result.SN == result.SAMAccountName
		if result.AccountType == E5KITStudent &&
			len(result.ProxyAddresses.Secondary) == 0 &&
			(result.GivenName == result.SN && result.GivenName == result.SAMAccountName && result.SN == result.SAMAccountName) {
			result.IsPseudonym = true
		}
		// append entry to results
		results = append(results, result)
	}
	return results, nil
}

func (s SearchResult) PrettyPrint() {
	// line: DN
	fmt.Println(styles.ColorBlue.Bold(true).Render(s.DN))

	// line: UAC
	var uacString string
	switch {
	case s.UserAccountControl.IsAccountDisabled():
		uacString += styles.ColorRed.Render(SymbolAccountDisabled + " Account disabled ")
	case s.UserAccountControl.IsPasswordExpired():
		uacString += styles.ColorYellow.Render(SymbolPasswordExpired + " Password expired ")
	}
	if uacString != "" {
		fmt.Println(styles.IndentOne.Render(uacString))
	}

	// Name + OE
	pseudonymEmoji := ""
	if s.IsPseudonym {
		pseudonymEmoji = styles.ColorForegroundGray.Render(SymbolPseudonym + " ")
	}
	fmt.Println(styles.IndentOne.Render(fmt.Sprintf(
		"%s %s %s %s (%s)",
		styles.TextBold.Render(styles.ColorForegroundStrong.Render(IndicateEmptyString(s.GivenName))),
		styles.TextBold.Render(styles.ColorForegroundStrong.Render(IndicateEmptyString(s.SN))),
		pseudonymEmoji+s.AccountType.ToEmoji(),
		IndicateEmptyString(s.Department),
		styles.ColorAqua.Render(quoteLeft+IndicateEmptyString(s.DisplayName)+quoteRight),
	)))

	// line: telephone and place
	var commData []string
	if len(s.PhysicalDeliveryOfficeName) > 0 {
		commData = append(commData, s.PhysicalDeliveryOfficeName)
	}
	if len(s.TelephoneNumber) > 0 {
		commData = append(commData, s.TelephoneNumber)
	}
	if len(s.PhysicalDeliveryOfficeName)+len(s.TelephoneNumber) > 0 {
		fmt.Println(styles.IndentOne.Render(strings.Join(commData, " "+delimiter+" ")))
	}

	// line: ITB OEs
	sort.Strings(s.ITB)
	if len(s.ITB) > 0 {
		fmt.Println(styles.IndentOne.Render(SymbolITB + " for " + strings.Join(s.ITB, " ")))
	}

	// line: account names
	fmt.Println(
		styles.IndentOne.Render(
			fmt.Sprintf("%s (UPN: %s)",
				styles.TextBold.Render(styles.ColorOrange.Render(s.SAMAccountName)),
				styles.ColorForeground.Render(s.UserPrincipalName)),
		),
	)

	// line: SubmissionContLength (if set)
	if s.SubmissionContLength != nil {
		fmt.Println(
			styles.IndentOne.Render(
				styles.ColorRed.Bold(true).Render(
					fmt.Sprintf("%s submissionContLength: %d", SymbolSendLimit, *s.SubmissionContLength))))
	}

	// line: e-mail-addresses
	if s.ProxyAddresses.Primary != "" {
		fmt.Println(styles.IndentOne.Render("E-Mail-Addresses:"))
		fmt.Println(styles.IndentTwo.Render(SymbolBulletPointPrimary + " " + styles.ColorGreen.Bold(true).Render(s.ProxyAddresses.Primary)))
		for _, secondary := range s.ProxyAddresses.Secondary {
			switch {
			case strings.HasPrefix(secondary, s.SAMAccountName+"@"):
				fmt.Println(styles.IndentTwo.Render(SymbolBulletPoint + " " + styles.TextItalic.Render(secondary)))
			default:
				fmt.Println(styles.IndentTwo.Render(SymbolBulletPoint + " " + styles.ColorForeground.Render(secondary)))
			}
		}
	}
	// e-mail redirect
	if s.TargetAddress != "" {
		fmt.Println(
			styles.IndentOne.Render(fmt.Sprintf("%s %s (redirect)", SymbolRedirect,
				styles.ColorPurple.Render(s.TargetAddress))))
	}
	// responsible person (if set)
	if s.E9 != "" {
		fmt.Println(styles.IndentOne.Render(fmt.Sprintf("%s (%s %s)", SymbolResponsiblePerson, SymbolRedirectAlt, styles.ColorOrange.Render(s.E9))))
	}

	// line: group members
	if !viper.GetBool("noMembers") && s.Members != nil && len(*s.Members) > 0 {
		fmt.Println(styles.IndentOne.Render("Group Members:"))
		for _, member := range *s.Members {
			fmt.Println(styles.IndentTwo.Render(SymbolBulletPoint + " " + member.SingleLineString()))
		}
	}

	// line: memberOf
	if viper.GetBool("memberof") && len(s.MemberOf) > 0 {
		fmt.Println(styles.IndentOne.Render("memberOf:"))
		for _, group := range s.MemberOf {
			fmt.Println(styles.IndentTwo.Render(styles.ColorForeground.Render(group)))
		}
	}

	// line: userCertificate
	if len(s.UserCertificate) > 0 {
		if len(s.UserCertificate) > 1 {
			fmt.Println(styles.IndentOne.Render("Certificates:"))
		} else {
			fmt.Println(styles.IndentOne.Render("Certificate:"))
		}
		// sort certificates by start date
		sort.Slice(s.UserCertificate, func(i, j int) bool {
			return s.UserCertificate[i].NotBefore.After(s.UserCertificate[j].NotBefore)
		})
		for _, cert := range s.UserCertificate {
			// short name for issuer
			var IssuerName string
			if cert.Issuer.CommonName == "KIT-CA" {
				if len(cert.Issuer.OrganizationalUnit) == 0 {
					IssuerName = styles.ColorPurple.Render("KIT-CA G2")
				} else {
					IssuerName = styles.ColorForegroundGray.Render("KIT-CA G1")
				}
			} else {
				IssuerName = styles.ColorCyan.Render(cert.Issuer.CommonName)
			}
			// start date
			var startDate = cert.NotBefore.Format("2006-01-02")
			if cert.Issuer.CommonName == "GEANT Personal CA 4" && cert.NotBefore.Year() == time.Now().Year() && cert.NotBefore.YearDay() == time.Now().YearDay() {
				// warn if there is a certificate from today
				startDate = styles.ColorRed.Bold(true).Render(SymbolCertificateFromToday + " " + startDate)
			}
			// end date
			var endDate string
			if cert.NotAfter.Before(time.Now()) {
				endDate = styles.ColorRed.Bold(true).Render(cert.NotAfter.Format("2006-01-02"))
			} else {
				endDate = styles.ColorGreen.Render(cert.NotAfter.Format("2006-01-02"))
			}
			// OU
			var ou string
			if len(cert.Subject.OrganizationalUnit) > 0 {
				ou = fmt.Sprintf("(%s%s%s) %s", quoteLeft, styles.ColorBlue.Render(strings.Join(cert.Subject.OrganizationalUnit, ", ")), quoteRight, delimiter)
			}
			// CN
			var cn = cert.Subject.CommonName
			if strings.Contains(cert.Subject.CommonName, "Uebergangszertifikat") {
				cn = styles.ColorYellow.Bold(true).Render(cert.Subject.CommonName)
			}

			// print first line
			fmt.Println(styles.IndentTwo.Render(
				strings.Join(
					slices.DeleteFunc(
						[]string{
							SymbolCertificate,
							quoteLeft + styles.TextBold.Render(IndicateEmptyString(cn)) + quoteRight,
							delimiter,
							ou,
							strings.Join(cert.EmailAddresses, ", "),
						}, func(s string) bool {
							return s == ""
						}),
					" ")))
			// print second line
			fmt.Println(styles.IndentThree.Render(
				strings.Join(
					slices.DeleteFunc(
						[]string{
							IssuerName,
							delimiter,
							startDate,
							"–",
							endDate,
						}, func(s string) bool {
							return s == ""
						}),
					" "),
			))
			// print third line
			fmt.Println(styles.IndentThree.Render(
				SymbolBulletPoint,
				EmphasizeStringEnds(cert.SerialNumber.String(), 4, &styles.ColorForeground)))
			// print fourth line
			fmt.Println(styles.IndentThree.Render(
				SymbolBulletPoint,
				styles.ColorForegroundGray.Render("0x")+
					EmphasizeStringEnds(cert.SerialNumber.Text(16), 4, &styles.ColorForeground)))
		}
	}
}

func (s SearchResult) SingleLineString() string {
	var r strings.Builder

	if len(s.SAMAccountName) > 0 {
		r.WriteString(styles.ColorOrange.Bold(true).Render(s.SAMAccountName) + " ")
	}
	if len(s.DisplayName) > 0 {
		r.WriteString(styles.ColorForegroundStrong.Render(s.DisplayName) + " ")
	}
	if len(s.ProxyAddresses.Primary) > 0 {
		r.WriteString("<" + styles.ColorGreen.Render(s.ProxyAddresses.Primary) + ">" + " ")
	}
	if len(s.TargetAddress) > 0 {
		r.WriteString("( " + SymbolRedirect + " " + styles.ColorPurple.Render(s.TargetAddress) + " )")
	}

	return r.String()
}

// IndicateEmptyString returns "∅" if the string is empty, otherwise the string itself.
func IndicateEmptyString(s string) string {
	if len(s) == 0 {
		return SymbolEmptyString
	}
	return s
}

// EmphasizeStringEnds returns a string with the first and last ›length‹ characters emphasized by ›style‹.
func EmphasizeStringEnds(s string, length int, style *lipgloss.Style) string {
	l := len(s)
	if l < 2*length {
		length = l / 2
	}

	return style.Render(s[:length]) + s[length:l-length] + style.Render(s[l-length:])
}
