package models

import (
	"encoding/json"
	"strconv"
	"strings"
)

type UAC int

const (
	UACAccountDisabled UAC = 0x00000002
	UACNormalAccount   UAC = 0x00000200
	UACPasswordExpired UAC = 0x00800000
)

func UACFromLDAPString(uac string) UAC {
	value, err := strconv.Atoi(uac)
	if err != nil {
		return 0
	}
	return UAC(value)
}

func (u UAC) IsAccountDisabled() bool {
	return u&UACAccountDisabled == UACAccountDisabled
}

func (u UAC) IsNormalAccount() bool {
	return u&UACNormalAccount == UACNormalAccount
}

func (u UAC) IsPasswordExpired() bool {
	return u&UACPasswordExpired == UACPasswordExpired
}

func (u UAC) ToString() string {
	return strconv.Itoa(int(u))
}

func (u UAC) ToEmoji() string {
	var e []string
	switch {
	case u.IsAccountDisabled():
		e = append(e, "💀")
	case u.IsNormalAccount():
		e = append(e, "👤") // 👥
	case u.IsPasswordExpired():
		e = append(e, "⌛")
	}
	return strings.Join(e, " ")
}

func (u UAC) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		RawValue        int  `json:"rawValue"`
		Disabled        bool `json:"disabled"`
		PasswordExpired bool `json:"passwordExpired"`
		NormalAccount   bool `json:"normalAccount"`
	}{
		RawValue:        int(u),
		Disabled:        u.IsAccountDisabled(),
		PasswordExpired: u.IsPasswordExpired(),
		NormalAccount:   u.IsNormalAccount(),
	})
}
