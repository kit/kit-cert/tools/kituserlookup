package models

const (
	delimiter                = "♦" // ⏺ ⏹ ⏵ 🍥 ♦ 🔸★ 🌟 ❇
	quoteLeft                = "»"
	quoteRight               = "«"
	SymbolRedirect           = "➜"
	SymbolRedirectAlt        = "⇝" // ➜ ⇝
	SymbolEmptyString        = "∅"
	SymbolBulletPoint        = "●" // ⁃ • ● ⏺
	SymbolBulletPointPrimary = "◉" // ○ ◌ ◎ ◉ ◇ ◈
	SymbolITB                = "💪" // 💪 🦁 👑 🫅 🥷
	SymbolCertificate        = "🔐" // 🔐 🔏 🖋 🪪 📜 📝

	SymbolResponsiblePerson    = "👤"
	SymbolAccountDisabled      = "💀"
	SymbolPasswordExpired      = "⌛"
	SymbolCertificateFromToday = "🚨"
	SymbolSendLimit            = "⇵"

	SymbolTypeKITStaff    = "👔" // 👔 👷 ⛑️ 🎩 🥼 🦺 💼 🧑‍💼️ 🥇
	SymbolTypeKITGuP      = "🤝" // 🙋🏻 ️🤝 🤲 🙌 🦯 🥈
	SymbolTypeKITStudent  = "📚" // 🎓 📚 🎒 🤓 🥉
	SymbolTypeKITAlumni   = "🎓" // 👴 🧔 🏃 🆓 🛸 🥷
	SymbolTypeKITFunction = "🤖" // 🤖 🤠 🤑 🤡
	SymbolTypeKITGroup    = "👥" // 👥 👨‍👩‍👧‍👦 🫂 👨‍👩‍👦‍
	SymbolTypeKITContact  = "📨" // 💬 📇 📔 🗃️ 📨 🗂️
	SymbolTypeKITUnknown  = "👾" // 🧟 🤷 ❓ 🤔 👽 👾 🕵 🤡

	SymbolPseudonym = "👻" // 🦸 🕴️ 👻 🕵️ ‍🌫️ 🌁️
)
