package models

import (
	"bytes"
	"regexp"
	"sort"
	"strings"
	"text/template"

	"github.com/go-ldap/ldap/v3"

	"gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/shortestprefix"
)

var (
	LDAPFilterShortestPrefix shortestprefix.ShortestPrefixResult
	AvailableLDAPFilterNames []string
	LDAPFilterNamesSorted    []string
)

func init() {
	// enumerate available filters
	AvailableLDAPFilterNames = make([]string, 0, len(LDAPFilter))
	for k := range LDAPFilter {
		AvailableLDAPFilterNames = append(AvailableLDAPFilterNames, k)
	}
	sort.Strings(AvailableLDAPFilterNames[:])
	// find shortest prefix for filter names
	LDAPFilterShortestPrefix = shortestprefix.ShortestPrefix(AvailableLDAPFilterNames)

	// sort filters by display priority
	LDAPFilterNamesSorted = make([]string, len(LDAPFilter))
	copy(LDAPFilterNamesSorted, AvailableLDAPFilterNames)
	sort.Slice(LDAPFilterNamesSorted, func(i, j int) bool {
		return LDAPFilter[LDAPFilterNamesSorted[i]].DisplayPriority > LDAPFilter[LDAPFilterNamesSorted[j]].DisplayPriority
	})
}

var (
	removeSpaceBetweenParentheses = regexp.MustCompile(`\)[[:space:]]+\(`)
	removeSpaceAfterOperator      = regexp.MustCompile(`\((?P<op>[!&|])[[:space:]]+`)
	removeSpaceBeforeParenthesis  = regexp.MustCompile(`[[:space:]]+\)`)
	matchKITPhoneNumber           = regexp.MustCompile(`^[248][[:digit:]]{4,}$`)
)

// compactFilter removes all spaces between parentheses and after operators
// to allow for readable filter expressions in code
func compactFilter(filter string) string {
	filter = removeSpaceBetweenParentheses.ReplaceAllString(filter, ")(")
	filter = removeSpaceAfterOperator.ReplaceAllString(filter, "(${op}")
	filter = removeSpaceBeforeParenthesis.ReplaceAllString(filter, ")")
	return strings.TrimSpace(filter)
}

type LDAPFilterTemplate struct {
	Template        *template.Template
	Description     string
	DisplayPriority int
}

var (
	// LDAPFilter is a map of named LDAP filter templates
	LDAPFilter = map[string]LDAPFilterTemplate{

		"default": {
			Template: template.Must(template.New("Default").Parse(compactFilter(`
		(|
			(sAMAccountName={{- .Query -}})
			(proxyAddresses=smtp:{{- .Query -}}*)
			(mail={{- .Query -}}*)
			(userPrincipalName={{- .Query -}}*)
			(targetAddress=smtp:{{- .Query -}}*)
			(extensionAttribute9={{- .Query -}}*)
			(givenName={{- .Query -}})
			(sn={{- .Query -}}))
		`))),
			Description:     "The default filter searches names, email addresses, account names and a few associated attributes. Account names and personal names need to be fully specified.",
			DisplayPriority: 100,
		},
		"nonames": {
			Template: template.Must(template.New("NoNames").Parse(compactFilter(`
		(|
			(sAMAccountName={{- .Query -}})
			(proxyAddresses=smtp:{{- .Query -}}*)
			(mail={{- .Query -}}*)
			(userPrincipalName={{- .Query -}}*)
			(targetAddress=smtp:{{- .Query -}}*)
			(extensionAttribute9={{- .Query -}}*)
		)`))),
			Description:     "The same as the default filter, but without searching for personal names.",
			DisplayPriority: 80,
		},
		"extensive": {
			Template: template.Must(template.New("Extensive").Parse(compactFilter(`
		(|
			(sAMAccountName={{- .Query -}})
			(proxyAddresses=smtp:{{- .Query -}}*)
			(mail={{- .Query -}}*)
			(userPrincipalName={{- .Query -}}*)
			(targetAddress=smtp:{{- .Query -}}*)
			(extensionAttribute9={{- .Query -}}*)
			(givenName=*{{- .Query -}}*)
			(sn=*{{- .Query -}}*)
			(displayName=*{{- .Query -}}*)
		)`))),
			Description: "Searches a lot of attributes with wildcards on both sides of the query. This filter is slow and should be used with caution.",
		},
		"email": {
			Template: template.Must(template.New("EMail").Parse(compactFilter(`
		(|
			(proxyAddresses=smtp:{{- .Query -}}*)
			(mail={{- .Query -}}*)
			(targetAddress=smtp:{{- .Query -}}*)
		)`))),
			Description:     "Only searches email addresses.",
			DisplayPriority: 70,
		},
		"phone": {
			Template: template.Must(template.New("Phone").Parse(compactFilter(`
			(telephoneNumber=*{{- .Query -}})
		`))),
			Description: "Only searches phone numbers without extension. This is automatically used if the query looks like an internal phone number. Only finds phone numbers that people have published. Use the internal phone search [2] for better results.",
		},
		"account": {
			Template: template.Must(template.New("PrimaryAccount").Parse(compactFilter(`
		(|
			(sAMAccountName={{- .Query -}})
			(userPrincipalName={{- .Query -}}*))
		`))),
			Description: "Only searches account names and user principal names.",
		},
		"distinguishedName": {
			Template: template.Must(template.New("DistinguishedName").Parse(compactFilter(`
			(distinguishedName={{- .Query -}})
		`))),
			Description: "Searches the (full) distinguished name of an object. This is useful for finding a specific object. Only returns no or exactly one result.",
		},
		"itb": {
			Template: template.Must(template.New("DistinguishedName").Parse(compactFilter(`
			(CN=SCC-ITB-{{- .Query -}}*)
		`))),
			Description:     "Looks for ITB account for specific OUs. Use only the OU name as query. Append a wildcard to the search (search »foo« will find »SCC-ITB-FOO« as well as »SCC-ITB-FOOBAR«).",
			DisplayPriority: 55,
		},
		"itbexact": {
			Template: template.Must(template.New("DistinguishedName").Parse(compactFilter(`
			(CN=SCC-ITB-{{- .Query -}})
		`))),
			Description:     "Like itb but only finds exact matches.",
			DisplayPriority: 50,
		},
		"externalRedirect": {
			Template: template.Must(template.New("ExternalRedirect").Parse(compactFilter(`
			(&
				(mail=*)
				(targetAddress=*)
				(!(targetAddress=*.kit.edu)))
		`))),
			Description:     "Searches for external redirect addresses. This filter is slow and should be used with caution. The query is not used.",
			DisplayPriority: 0,
		},
	}
)

// GetLDAPFilter returns a named LDAP filter string with the query already inserted
func GetLDAPFilter(name, query string) string {
	var t *template.Template

	// if query looks like a phone number, switch to the phone search template
	if name == "default" && matchKITPhoneNumber.MatchString(query) {
		name = "phone"
	}

	// find ldap filter based on query
	filtername, err := LDAPFilterShortestPrefix.Match(name)
	if err != nil {
		filtername = "default"
	}

	// default to default filter
	if t = LDAPFilter[filtername].Template; t == nil {
		t = LDAPFilter["default"].Template
	}

	// insert query into filter expression
	var b bytes.Buffer
	_ = t.Execute(&b, struct{ Query string }{ldap.EscapeFilter(query)})
	return b.String()
}
