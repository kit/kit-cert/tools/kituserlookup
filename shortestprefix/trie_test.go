package shortestprefix

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShortestPrefix(t *testing.T) {
	words := []string{"dog", "dogfish", "apple", "apricot", "fish", "dogfood", "doggy"}
	result := ShortestPrefix(words)

	tests := []struct {
		input        string
		expected     string
		returnsError bool
	}{
		{"dog", "dog", false},
		{"dogfi", "dogfish", false},
		{"doo", "", true},
		{"f", "fish", false},
		{"xxx", "", true},
		{"applesauce", "apple", false},
	}

	for _, test := range tests {
		match, hasMatch := result.Match(test.input)
		if test.returnsError {
			assert.Error(t, hasMatch, "expected error for input "+test.input)
		} else {
			assert.NoError(t, hasMatch, "expected no error for input "+test.input)
			assert.Equal(t, test.expected, match, "shortest prefix does not match expected value for input "+test.input)
		}
	}
}
