package shortestprefix

import (
	"errors"
	"sort"
	"strings"

	"github.com/charmbracelet/lipgloss"
)

type Trie struct {
	Children    map[rune]*Trie
	InsertCount int
}

// NewTrie creates a new Trie
func NewTrie() *Trie {
	return &Trie{
		Children: make(map[rune]*Trie),
	}
}

// Insert a string into the trie
func (t *Trie) Insert(s string) {
	t.InsertCount++
	if s == "" {
		return
	}
	r := rune(s[0])
	if _, ok := t.Children[r]; !ok {
		t.Children[r] = NewTrie()
	}
	t.Children[r].Insert(s[1:])
}

// ShortestPrefix returns the shortest prefix of a string that is unique in the trie
func (t *Trie) ShortestPrefix(s string) string {
	if s == "" {
		return ""
	}
	r := rune(s[0])
	if t.Children[r].InsertCount == 1 {
		return string(r)
	}
	return string(r) + t.Children[r].ShortestPrefix(s[1:])
}

type ShortestPrefixResult struct {
	PrefixMap    map[string]string
	LongestFirst []string
}

// Match returns the string that matches the longest prefix of q and a boolean indicating if a match was found
func (s ShortestPrefixResult) Match(q string) (string, error) {
	for _, v := range s.LongestFirst {
		if strings.HasPrefix(q, v) {
			return s.PrefixMap[v], nil
		}
	}
	return "", errors.New("no match " + q + " found")
}

// FancyString returns a string with the shortest prefixes highlighted
func (s ShortestPrefixResult) FancyString(style lipgloss.Style) string {
	var list = make([]string, 0, len(s.PrefixMap))
	for prefix, name := range s.PrefixMap {
		list = append(list, style.Render(prefix)+name[len(prefix):])
	}
	return strings.Join(list, ", ")
}

// ShortestPrefix takes a slice of strings and returns a map of shortest prefixes plus a sorted version of the input strings
func ShortestPrefix(s []string) ShortestPrefixResult {
	r := make(map[string]string)
	// insert all strings into trie
	t := NewTrie()
	for _, v := range s {
		t.Insert(v)
	}
	// create a map of shortest prefixes
	for _, v := range s {
		r[t.ShortestPrefix(v)] = v
	}
	// sort input strings such that same prefixes always have the longer string first (e.g. dogfood before dog)
	lp := make([]string, 0, len(s))
	for key := range r {
		lp = append(lp, key)
	}
	sort.Slice(lp, func(i, j int) bool {
		// if j is a prefix of i, sort i first
		if strings.HasPrefix(lp[i], lp[j]) {
			return true
		}
		// if i is a prefix of j, sort j first
		if strings.HasPrefix(lp[j], lp[i]) {
			return false
		}
		return lp[i] < lp[j]
	})

	return ShortestPrefixResult{
		PrefixMap:    r,
		LongestFirst: lp,
	}
}
