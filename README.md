[![pipeline status](https://gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/badges/main/pipeline.svg)](https://gitlab.kit.edu/kit/kit-cert/tools/kituserlookup/commits/main)

Create `~/.config/kituserlookup.toml` and edit accordingly:

```toml
[ldap]
user           = "ab9999@kit.edu"
pass           = "Furzbembel"
#passcmd       = ["pass", "Accounts/ab9999"]
```

`passcmd` executes the given command and uses the first line of stdout as the password. The first argument is the command,
the following strings are options. The command is executed directly without a shell or any form of variable substitution.
It overrides `pass` when specified.

## Note to Windows users

Get a decent terminal emulator like [mintty](https://mintty.github.io/) via [MSYS2](https://www.msys2.org/) or [cygwin](https://cygwin.com/).
or a modern [PowerShell](https://github.com/PowerShell/PowerShell?tab=readme-ov-file#get-powershell).

Install a decent typeface like [Iosevka](https://be5invis.github.io/Iosevka/) or [mononoki](https://madmalik.github.io/mononoki/).
Get the [Nerd Font version](https://www.nerdfonts.com/) if you use advanced prompts like Powerline, ohmyposh or [starship](https://starship.rs/).
Also install a nice (color) emoji font like [Noto Color Emoji](https://fonts.google.com/noto/specimen/Noto+Color+Emoji).

Get a nice (dark) [color scheme](https://github.com/oumu/mintty-color-schemes).

Installing color scheme for PowerShell is a royal pain, you may try [Oh My Posh!](https://ohmyposh.dev/docs/themes)
which includes theming support.
