package gruppenverwaltung

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/cookiejar"
	"net/url"

	"golang.org/x/net/publicsuffix"
)

/*
Neue API:
https://scc-app-01.scc.kit.edu/itbportal-rest-war/rest/apiee/index.html
https://scc-app-01.scc.kit.edu/itb
*/

const (
	GVITBRoot                 = `SCC-ITB`
	GVApiServer               = `itb-backend.scc.kit.edu`
	GVListGroupsTemplate      = `/itbportal-rest-war/rest/groups/%s`
	GVSccItbInfoTemplate      = `/itbportal-rest-war/rest/groups/%s/%s`
	GVSccItbEffectiveTemplate = `/itbportal-rest-war/rest/groups/%s/%s/effectivemembers`
	GVGetIdentityTemplate     = `/itbportal-rest-war/rest/get/%s`
)

type GVClient struct {
	username, password string
	client             *http.Client
	baseurl            url.URL
}

type Group struct {
	Name        string
	OE          string
	Description string
	GidNumber   int
}

func NewGVCLient(baseurl, username, password string) (*GVClient, error) {
	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return nil, err
	}

	return &GVClient{
		client: &http.Client{
			Jar: jar,
		},
		baseurl: url.URL{
			Host:   GVApiServer,
			Scheme: "https",
			User:   url.UserPassword(username, password),
		},
		username: username,
		password: password,
	}, nil
}

func (c *GVClient) doRequest(template string, args ...interface{}) (interface{}, error) {
	var (
		result []Group
	)
	// build URL
	baseurl := c.baseurl
	baseurl.Path = fmt.Sprintf(template, args...)
	// build request
	req, err := http.NewRequest("GET", baseurl.String(), nil)
	if err != nil {
		return result, err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", "kituserlookup")
	// make request
	resp, err := c.client.Do(req)
	if err != nil {
		return result, err
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (c *GVClient) GetGroups(root string) (interface{}, error) {
	return c.doRequest(GVListGroupsTemplate, GVITBRoot)
}
