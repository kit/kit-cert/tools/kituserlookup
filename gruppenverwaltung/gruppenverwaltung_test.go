package gruppenverwaltung

import (
	"encoding/json"
	"log"
	"os"
	"testing"

	"github.com/mitchellh/go-homedir"
)

type Credentials struct {
	Username string
	Password string
}

var (
	client             *GVClient
	credentialFilename string
)

func init() {
	var err error
	credentialFilename, err = homedir.Expand("~/.config/scc-tools/credentials_gruppenverwaltung_readonly.json")
	if err != nil {
		log.Fatalf("Unable to open credential file: %s", err)
	}
}

func TestNewGVCLient(t *testing.T) {
	var (
		credentialRaw []byte
		err           error
		creds         Credentials
	)
	credentialRaw, err = os.ReadFile(credentialFilename)
	if err != nil {
		t.Error(err)
	}
	err = json.Unmarshal(credentialRaw, &creds)
	if err != nil {
		t.Error(err)
	}
	client, err = NewGVCLient(GVApiServer, creds.Username, creds.Password)
	if err != nil {
		t.Error(err)
	}
}

func TestGVClient_GetGroups(t *testing.T) {
	_, err := client.GetGroups(GVITBRoot)
	if err != nil {
		t.Error(err)
	}
}
